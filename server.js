'use strict';

const Promise = require('bluebird');
global._ = require('lodash');

const exec = Promise.promisifyAll(require('child_process')).execAsync;
const pm2 = Promise.promisifyAll(require('pm2'));
const restify = require('restify');

const config = require('./config');
const info = require('./package.json');
const git = require('./lib/git');


const server = restify.createServer({
  name: info.name,
  version: info.version,
});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.queryParser());
server.use(restify.bodyParser());

const stack = [];

server.get('/', (req, res, next) => {
  if (req.authorization && req.authorization.basic &&
    req.authorization.basic.password === config.password) {

    return next();
  }
  else {
    res.setHeader('WWW-Authenticate', 'Basic');
    res.status(401);
    return res.end('No');
  }

},
(req, res) => {
  res.header('Content-Type', 'text/html');
  return res.end('Not implemented yet. <a href="https://gitlab.com/bentinata/wimmersperg.atom">Get updates!</a>');
})

server.post('/hook', (req, res) => {
  const body = req.body;
  const trigger = git[git.determineHost(body)](body);

  const app = _.find(stack, repo => git.compare(repo.git, trigger));

  if (app) {
    // Don't make triggerer wait
    // Their job is done
    res.send(200);

    const opt = {
      cwd: app.path,
    };

    exec('git pull origin ' + app.git.branch, opt)
      .then(() => pm2.restartAsync(app.pm2.id))
      .then(process => {
        console.log('Repository', app.git.repo, 'updated');
      })
      .catch(console.log);
  }
  else {
    res.send(500);
  }
});

// Start http server after list of process acquired
pm2.connectAsync()
  .then(() => pm2.listAsync())
  .then(processes => {
    processes.forEach(process => stack.push(git.process(process)));
  })
  .then(() => {
    server.listen(config.port)
  })
  .then(() => {
    console.log('Wimmersperg listen on', config.port);
  })
  .catch(function(err) {
    console.log(err);
  })
