'use strict';

function resolveRemote(uri) {
  let split = uri.split(/[@:\/]/).filter(e => e != '');

  return {
    protocol: split[0].match(/http(s)/) ? split[0] : 'ssh',
    host: split[1],
    user: split[2],
    repo: split[3].replace('.git', ''),
  };
}

function compare(x, y) {
  if (typeof x === 'string')
    x = resolveRemote(x);

  if (typeof y === 'string')
    y = resolveRemote(y);

  return (x.host === y.host &&
      x.user === y.user &&
      x.repo === y.repo)
}

function process(process) {
  const versioning = process.pm2_env.versioning;

  const app = {
    path: versioning.repo_path,
    git: _.merge({branch: versioning.branch}, resolveRemote(versioning.url)),
    pm2: {
      id: process.pm_id,
    },
  }

  return app;
}

function determineHost(body) {
  if (body.repository.url)
    return 'gitlab';

  return 'bitbucket';
}

function bitbucket(body) {
  return resolveRemote(body.repository.links.html.href);
}

function gitlab(body) {
  return resolveRemote(body.repository.url);
}

module.exports = {
  resolveRemote,
  compare,
  process,
  determineHost,
  bitbucket,
  gitlab,
}
